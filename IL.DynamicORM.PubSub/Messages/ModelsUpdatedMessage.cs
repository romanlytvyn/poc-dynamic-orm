﻿using System;
using System.Collections.Generic;

namespace IL.DynamicORM.PubSub.Messages
{
	public class ModelsUpdatedMessage
	{
		public string Key { get; }

		public IEnumerable<string> Tables { get; }

		public ModelsUpdatedMessage(string key, IEnumerable<string> tables)
		{
			Key = key;
			Tables = tables;
		}

		public override string ToString()
		{
			return $"{{Key={Key};Tables=[{String.Join(",", Tables)}]}}";
		}
	}
}
