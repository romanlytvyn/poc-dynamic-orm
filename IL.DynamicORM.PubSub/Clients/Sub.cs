﻿using System;
using System.Text;
using IL.DynamicORM.PubSub.Clients.Base;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace IL.DynamicORM.PubSub.Clients
{
	public class Sub : BaseClient
	{
		private IConnection _connection;
		private IModel _channel;

		public Sub(string hostName = "localhost", string userName = "guest", string password = "guest")
			: base(hostName, userName, password)
		{
		}

		public bool StartListening<TMessage>(Action<TMessage> callback, string exchange = "il.dynamic.orm")
		{
			try
			{
				StopListening();

				_connection = Factory.CreateConnection();
				_channel = _connection.CreateModel();
				_channel.ExchangeDeclare(exchange: exchange, type: "fanout", durable: false, autoDelete: true);

				var queueName = _channel.QueueDeclare().QueueName;
				_channel.QueueBind(queue: queueName, exchange: exchange, routingKey: "");

				var consumer = new EventingBasicConsumer(_channel);
				consumer.Received += (model, ea) =>
				{
					var message = JsonConvert.DeserializeObject<TMessage>(Encoding.UTF8.GetString(ea.Body));
					callback(message);
				};

				_channel.BasicConsume(queue: queueName, noAck: true, consumer: consumer);

				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public void StopListening()
		{
			_channel?.Dispose();
			_connection?.Dispose();
		}
	}
}
