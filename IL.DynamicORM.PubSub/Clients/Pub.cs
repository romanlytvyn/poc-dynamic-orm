﻿using System;
using System.Text;
using IL.DynamicORM.PubSub.Clients.Base;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace IL.DynamicORM.PubSub.Clients
{
	public class Pub : BaseClient
	{
		public Pub(string hostName = "localhost", string userName = "guest", string password = "guest")
			: base(hostName, userName, password)
		{
		}

		public bool Emit<TMessage>(TMessage message, string exchange = "il.dynamic.orm")
		{
			try
			{
				using (var connection = Factory.CreateConnection())
				{
					using (var channel = connection.CreateModel())
					{
						channel.ExchangeDeclare(exchange: exchange, type: "fanout", durable: false, autoDelete: true);

						var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message));
						channel.BasicPublish(exchange: exchange, routingKey: "", basicProperties: null, body: body);
					}
				}
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}
	}
}
