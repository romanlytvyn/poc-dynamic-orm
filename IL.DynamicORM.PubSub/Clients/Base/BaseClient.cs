﻿using RabbitMQ.Client;

namespace IL.DynamicORM.PubSub.Clients.Base
{
	public abstract class BaseClient
	{
		protected readonly ConnectionFactory Factory;

		protected BaseClient(string hostName = "localhost", string userName = "guest", string password = "guest")
		{
			Factory = new ConnectionFactory
			{
				HostName = hostName,
				UserName = userName,
				Password = password
			};
		}
	}
}
