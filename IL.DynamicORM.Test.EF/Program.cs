﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using IL.DynamicORM.Core.Linq;
using IL.DynamicORM.EF.DbContext;
using IL.DynamicORM.EF.Linq;
using IL.DynamicORM.PubSub.Clients;
using IL.DynamicORM.PubSub.Constants;
using IL.DynamicORM.PubSub.Messages;
using IL.DynamicORM.Test.EF.Entities;
using Z.EntityFramework.Plus;

namespace IL.DynamicORM.Test.EF
{
	static class Program
	{
		private static Pub _pub;
		private static Sub _sub;

		private static void Main()
		{
			DynamicDbContextInfoFactory.Initialize(new Dictionary<string, string>
			{
				{"DynamicORM", ConfigurationManager.ConnectionStrings["DynamicORM"].ConnectionString },
				{"DynamicORM2", ConfigurationManager.ConnectionStrings["DynamicORM2"].ConnectionString }
			}, typeof(DynamicORMContext).Namespace);

			_pub = new Pub();
			_sub = new Sub();

			SubscribeForModelsUpdate();

			while (true)
			{
				try
				{
					Console.WriteLine("-----------------------------------------------------------");
					Console.Write($"[LU] - list Users from DynamicORM{Environment.NewLine}" +
									$"[LU2] - list Users from DynamicORM2{Environment.NewLine}" +
									$"[LL] - list Logs from DynamicORM{Environment.NewLine}" +
									$"[FU] - find User in DynamicORM{Environment.NewLine}" +
									$"[AU] - add User (identity) to DynamicORM{Environment.NewLine}" +
									$"[AL] - add Log (no identity) to DynamicORM{Environment.NewLine}" +
									$"[DU] - delete User by Id (bulk) from DynamicORM{Environment.NewLine}" +
									$"[U] - update models{Environment.NewLine}" +
									$"[E] - exit{Environment.NewLine}" +
									"Enter action: ");
					var action = Console.ReadLine()?.ToLower();

					Console.WriteLine("-----------------------------------------------------------");

					switch (action)
					{
						case "lu":
							ListUsers();
							break;
						case "lu2":
							ListUsers2();
							break;
						case "ll":
							ListLogs();
							break;
						case "fu":
							FindUser();
							break;
						case "au":
							AddUser();
							break;
						case "al":
							AddLog();
							break;
						case "du":
							DeleteUser();
							break;
						case "u":
							UpdateModels();
							break;
						case "e":
							_sub.StopListening();
							return;
					}

					Console.WriteLine("-----------------------------------------------------------");
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex);
				}
			}
		}

		private static void ListUsers()
		{
			using (var db = new DynamicORMContext("DynamicORM"))
			{
				foreach (var item in db.Users/*.Where(u => u.UserInfo != null && u.UserInfo.Id == 1)*/)
				{
					foreach (var prop in item.GetType().GetProperties())
					{
						Console.Write("{0}={1};", prop.Name, prop.GetValue(item, null));
					}
					Console.WriteLine();
				}
			}
		}

		private static void ListUsers2()
		{
			using (var db = new DynamicORMContext("DynamicORM2"))
			{
				foreach (var item in db.Users)
				{
					foreach (var prop in item.GetType().GetProperties())
					{
						Console.Write("{0}={1};", prop.Name, prop.GetValue(item, null));
					}
					Console.WriteLine();
				}
			}
		}

		private static void ListLogs()
		{
			using (var db = new DynamicORMContext("DynamicORM"))
			{
				foreach (var item in db.Logs)
				{
					foreach (var prop in item.GetType().GetProperties())
					{
						Console.Write("{0}={1};", prop.Name, prop.GetValue(item, null));
					}
					Console.WriteLine();
				}
			}
		}

		private static void FindUser()
		{
			using (var db = new DynamicORMContext("DynamicORM"))
			{
				Console.Write("Enter property name: ");
				var property = Console.ReadLine() ?? String.Empty;

				Console.Write("Enter property value: ");
				object value = Console.ReadLine() ?? String.Empty;

				if (new[] {typeof(int), typeof(int?)}.Contains(db.GetActualType<UserBaseEntity>().GetProperty(property)?.PropertyType))
				{
					value = String.Empty.Equals(value) ? (object) null : Int32.Parse(value.ToString());
				}

				foreach (var item in db.Users
					.Where(EfLinqBuilder.ConstructWherePredicate<UserBaseEntity>(db.GetActualType<UserBaseEntity>(), new LinqCriteriaField(property, value))))
				{
					foreach (var prop in item.GetType().GetProperties())
					{
						Console.Write("{0}={1};", prop.Name, prop.GetValue(item, null));
					}
					Console.WriteLine();
				}
			}
		}

		private static void AddUser()
		{
			using (var db = new DynamicORMContext("DynamicORM"))
			{
				var user = db.CreateInstance<UserBaseEntity>();

				foreach (var prop in user.GetType().GetProperties()
					.Where(p => new[] { typeof(int), typeof(int?), typeof(string) }.Contains(p.PropertyType) 
						&& p.GetCustomAttributes(typeof(KeyAttribute), true).FirstOrDefault() == null))
				{
					Console.Write($"Enter {prop.Name} ({prop.PropertyType}): ");

					var input = Console.ReadLine() ?? String.Empty;

					if (prop.PropertyType == typeof(int))
					{
						prop.SetValue(user, input != String.Empty ? Int32.Parse(input) : 0);
					}
					else if (prop.PropertyType == typeof(int?))
					{
						prop.SetValue(user, input != String.Empty ? (ValueType) Int32.Parse(input) : null);
					}
					else
					{
						prop.SetValue(user, input);
					}
				}

				db.Users.Add(user);
				db.SaveChanges();

				Console.WriteLine($"Added successfuly, Id={user.Id}");
			}
		}

		private static void AddLog()
		{
			using (var db = new DynamicORMContext("DynamicORM"))
			{
				var log = new Log
				{
					Text = DateTime.Now.Ticks.ToString()
				};

				var result = db.SaveNewInstance(log);

				Console.WriteLine(result ? $"Added successfuly, Id={log.Id}" : "Error saving");
			}
		}

		private static void DeleteUser()
		{
			using (var db = new DynamicORMContext("DynamicORM"))
			{
				Console.Write("Enter user Id: ");
				var input = Console.ReadLine() ?? String.Empty;
				var id = Int32.Parse(input);

				var userToDelete = db.Users.Find(id);
				if (userToDelete != null)
				{
					db.Users
						.Where(u => u.Id == id)
						.Delete();

					Console.WriteLine("Deleted successfuly");
				}
				else
				{
					Console.WriteLine("Entry not found");
				}
			}
		}

		private static void UpdateModels()
		{
			var message = new ModelsUpdatedMessage("DynamicORM", new[] {"Users"});
			var result = _pub.Emit(message, Exchanges.StructureUpdated);

			Console.WriteLine(result ? $"Message sent: {message}" : "Could not send a message");
		}

		private static void SubscribeForModelsUpdate()
		{
			var listening = _sub.StartListening<ModelsUpdatedMessage>(message =>
			{
				Console.WriteLine();
				Console.WriteLine("-----------------------------------------------------------");
				Console.WriteLine($"Message received: {message}");
				Console.WriteLine("Updating models...");

				DynamicDbContextInfoFactory.RefreshDynamicDbContextInfo(message.Key, message.Tables);

				Console.WriteLine("Models updated");
				Console.WriteLine("-----------------------------------------------------------");
			}, Exchanges.StructureUpdated);

			Console.WriteLine(listening ? "Started Listening..." : "Could not start listening");
		}
	}
}
