using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IL.DynamicORM.Test.EF.Entities
{
	[Table("Users")]
	public abstract class UserBaseEntity
	{
		[Key]
		public int Id { get; set; }

		[Column("Name")]
		[StringLength(255)]
		public string UserName { get; set; }

		public int UserInfoId { get; set; }

		[ForeignKey("UserInfoId")]
		public virtual UserInfo UserInfo { get; set; }
	}
}
