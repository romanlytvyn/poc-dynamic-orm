using System.Data.Entity;
using IL.DynamicORM.EF.DbContext;

namespace IL.DynamicORM.Test.EF.Entities
{
	public class DynamicORMContext : DynamicDbContext
	{
		public DynamicORMContext(string key)
			: base(DynamicDbContextInfoFactory.GetDynamicDbContextInfo(key))
		{
		}

		public DbSet<UserBaseEntity> Users => DynamicSet<UserBaseEntity>();

		public DbSet<Log> Logs => Set<Log>();
	}
}
