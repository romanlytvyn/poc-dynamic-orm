using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IL.DynamicORM.Test.EF.Entities
{
	[Table("UsersInfo")]
	public class UserInfo
	{
		[Key]
		[Column("UserId")]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public int Id { get; set; }

		public string Info { get; set; }

		public override string ToString()
		{
			return $"{{Id={Id},Info={Info}}}";
		}
	}
}
