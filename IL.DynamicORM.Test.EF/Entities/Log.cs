using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IL.DynamicORM.Test.EF.Entities
{
	[Table("Logs")]
	public class Log
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public int Id { get; set; }

		[StringLength(255)]
		public string Text { get; set; }
	}
}
