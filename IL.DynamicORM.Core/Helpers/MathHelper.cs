﻿namespace IL.DynamicORM.Core.Helpers
{
	public static class MathHelper
	{
		public static object Increment(object value)
		{
			object incrementedValue = null;

			var valueType = value.GetType();
			
			if (valueType == typeof(short))
			{
				incrementedValue = (short)value + 1;
			}
			else if (valueType == typeof(ushort))
			{
				incrementedValue = (ushort)value + 1;
			}
			else if (valueType == typeof(int))
			{
				incrementedValue = (int)value + 1;
			}
			else if (valueType == typeof(uint))
			{
				incrementedValue = (uint)value + 1;
			}
			else if (valueType == typeof(long))
			{
				incrementedValue = (long)value + 1;
			}
			else if (valueType == typeof(ulong))
			{
				incrementedValue = (ulong)value + 1;
			}
			else if (valueType == typeof(decimal))
			{
				incrementedValue = (decimal)value + 1;
			}

			return incrementedValue;
		}
	}
}
