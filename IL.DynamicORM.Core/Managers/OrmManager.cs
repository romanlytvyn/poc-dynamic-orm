﻿using System.Collections.Generic;
using IL.DynamicORM.Core.Providers;

namespace IL.DynamicORM.Core.Managers
{
	public static class OrmManager
	{
		private static readonly Dictionary<string, ORMInfoProvider> Providers = new Dictionary<string, ORMInfoProvider>();

		public static ORMInfoProvider GetProvider(string key, string entitiesNamespace, string connectionString, bool refresh = false, IEnumerable<string> refreshTables = null)
		{
			if (!Providers.ContainsKey(key))
			{
				var provider = new ORMInfoProvider(key, entitiesNamespace, connectionString);
				Providers.Add(key, provider);
			}
			else if (refresh)
			{
				Providers[key].Refresh(refreshTables);
			}
			return Providers[key];
		}
	}
}
