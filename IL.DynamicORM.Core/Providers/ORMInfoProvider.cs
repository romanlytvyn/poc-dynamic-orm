﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using IL.DynamicORM.Core.Converters;
using IL.DynamicORM.Core.Extensions;
using IL.DynamicORM.Core.Factories;

namespace IL.DynamicORM.Core.Providers
{
	public class ORMInfoProvider
	{
		public readonly ConcurrentDictionary<string, Type> Tables = new ConcurrentDictionary<string, Type>();

		private readonly ConcurrentDictionary<Type, Type> _types = new ConcurrentDictionary<Type, Type>();
		private readonly string _key;
		private readonly string _entitiesNamespace;
		private readonly string _connectionString;

		private string DynamicAssemblyName => $"{_entitiesNamespace}.{_key}.Dynamic.Objects";

		public ORMInfoProvider(string key, string entitiesNamespace, string connectionString)
		{
			_key = key;
			_entitiesNamespace = entitiesNamespace;
			_connectionString = connectionString;

			Refresh();
		}

		public TBase CreateInstance<TBase>()
		{
			return (TBase) Activator.CreateInstance(GetActualType<TBase>());
		}

		public Type GetActualType<TBase>()
		{
			return _types[typeof(TBase)];
		}

		public void Refresh(IEnumerable<string> tables = null)
		{
			var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(t => t.GetTypes()).Where(t => t.IsClass && t.Namespace == _entitiesNamespace && t.Name.EndsWith("BaseEntity"));
			var dcf = new DynamicClassFactory(DynamicAssemblyName);

			Parallel.ForEach(types, baseType =>
			{
				var tableName = baseType.GetAttributeValue<TableAttribute, string>(a => a.Name);

				if (!String.IsNullOrEmpty(tableName) && (tables == null || tables.Contains(tableName)))
				{
					var properties = GetColumnsInfo(tableName);
					var type = dcf.CreateDynamicType(baseType.Name.Replace("BaseEntity", String.Empty), baseType, properties);

					Tables[tableName] = type;
					_types[baseType] = type;
				}
			});
		}

		private Dictionary<string, Type> GetColumnsInfo(string tableName)
		{
			var columns = new Dictionary<string, Type>();

			using (var connection = new SqlConnection())
			{
				connection.ConnectionString = _connectionString;
				connection.Open();

				using (var command = new SqlCommand())
				{
					command.Connection = connection;
					command.CommandText = $"exec sp_columns @table_name = '{tableName}'";
					command.CommandType = CommandType.Text;

					using (var reader = command.ExecuteReader())
					{
						if (reader.HasRows)
						{
							while (reader.Read())
							{
								columns.Add(reader["COLUMN_NAME"].ToString(), DbTypeConverter.ToSystemType(reader["TYPE_NAME"].ToString(), reader["NULLABLE"].ToString() == "1"));
							}
						}
					}
				}
			}

			return columns;
		}
	}
}
