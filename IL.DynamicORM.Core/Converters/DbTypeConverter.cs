﻿using System;

namespace IL.DynamicORM.Core.Converters
{
	public static class DbTypeConverter
	{
		public static Type ToSystemType(string dbType, bool isNullable)
		{
			dbType = dbType.ToLower().Replace("identity", String.Empty).Trim();

			switch (dbType)
			{
				case "nvarchar":
				case "varchar":
				case "ntext":
				case "text":
					return typeof(string);
				case "int":
					return isNullable ? typeof(int?) : typeof(int);
			}

			return typeof(object);
		}
	}
}
