﻿using System;
using System.Linq;
using System.Reflection;

namespace IL.DynamicORM.Core.Extensions
{
	public static class PropertyInfoExtensions
	{
		public static TValue GetAttributeValue<TAttribute, TValue>(this PropertyInfo propertyInfo, Func<TAttribute, TValue> valueSelector)
			where TAttribute : Attribute
		{
			var attribute = propertyInfo.GetCustomAttributes(typeof(TAttribute), true).FirstOrDefault();

			if (attribute != null)
			{
				return valueSelector(attribute as TAttribute);
			}

			return default(TValue);
		}
	}
}
