﻿using System;
using System.Linq;
using System.Reflection;

namespace IL.DynamicORM.Core.Extensions
{
	public static class TypeExtensions
	{
		public static TValue GetAttributeValue<TAttribute, TValue>(this Type type, Func<TAttribute, TValue> valueSelector)
			where TAttribute : Attribute
		{
			var attribute = type.GetCustomAttributes(typeof(TAttribute), true).FirstOrDefault();

			if (attribute != null)
			{
				return valueSelector(attribute as TAttribute);
			}

			return default(TValue);
		}

		public static PropertyInfo GetPropertyInfoByAttribute<TAttribute>(this Type type)
			where TAttribute : Attribute
		{
			return type
				.GetProperties()
				.Select(propertyInfo => new
				{
					propertyInfo,
					attribute = propertyInfo
									.GetCustomAttributes(typeof(TAttribute), true)
									.FirstOrDefault()
				})
				.Select(o => o.attribute != null ? o.propertyInfo : null)
				.FirstOrDefault();
		}
	}
}
