﻿using System;
using System.Linq.Expressions;

namespace IL.DynamicORM.Core.Linq
{
	public static class LinqBuilder
	{
		public static Expression<Func<TBase, bool>> ConstructWherePredicate<TBase>(Type type, EPropertyExpression propertyExpression, params LinqCriteriaField[] criteria)
		{
			Expression combined = null;

			var param = Expression.Parameter(typeof(TBase), typeof(TBase).Name);

			if (criteria != null)
			{
				foreach (var criteriaField in criteria)
				{
					MemberExpression columnNameProperty;

					switch (propertyExpression)
					{
						case EPropertyExpression.Convert:
							columnNameProperty = Expression.Property(Expression.TypeAs(param, type), criteriaField.Name);
							break;
						case EPropertyExpression.Create:
							columnNameProperty = Expression.Property(Expression.Parameter(type, type.Name), criteriaField.Name);
							break;
						default:
							columnNameProperty = Expression.Property(param, criteriaField.Name);
							break;
					}

					var propertyType = Nullable.GetUnderlyingType(columnNameProperty.Type) ?? columnNameProperty.Type;
					var value = criteriaField.Value == null ? null : Convert.ChangeType(criteriaField.Value, propertyType);
					var columnValue = Expression.Constant(value);

					var equal = Expression.Equal(columnNameProperty, columnValue);

					combined = combined == null ? equal : Expression.And(combined, equal);
				}
			}

			return combined != null ? Expression.Lambda<Func<TBase, bool>>(combined, param) : null;
		}

		public static Expression<Func<TBase, bool>> ConstructWherePredicate<TBase>(params LinqCriteriaField[] criteria)
		{
			return ConstructWherePredicate<TBase>(typeof(object), EPropertyExpression.Simple, criteria);
		}

		public static Expression<Func<TInstance, TResult>> ConstructSelector<TInstance, TResult>(string propertyName)
		{
			var parameterExpression = Expression.Parameter(typeof(TInstance), typeof(TInstance).Name);
			var memberExpression = Expression.Property(parameterExpression, propertyName);
			var lambdaExpression = Expression.Lambda(memberExpression, parameterExpression);

			return (Expression<Func<TInstance, TResult>>)lambdaExpression;
		}
	}
}
