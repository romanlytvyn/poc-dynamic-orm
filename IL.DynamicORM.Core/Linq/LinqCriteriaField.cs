﻿namespace IL.DynamicORM.Core.Linq
{
	public class LinqCriteriaField
	{
		public string Name { get; }

		public object Value { get; }

		public string Operator { get; }

		public LinqCriteriaField(string name, object value, string @operator = "=")
		{
			Name = name;
			Value = value;
			Operator = @operator;
		}
	}
}
