﻿namespace IL.DynamicORM.Core.Linq
{
	public enum EPropertyExpression
	{
		Simple = 0,
		Convert = 1,
		Create = 2
	}
}
