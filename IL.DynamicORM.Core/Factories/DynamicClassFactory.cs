﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading;
using IL.DynamicORM.Core.Extensions;

namespace IL.DynamicORM.Core.Factories
{
	public class DynamicClassFactory
	{
		private readonly AppDomain _appDomain;
		private readonly string _assemblyName;
		private AssemblyBuilder _assemblyBuilder;
		private ModuleBuilder _moduleBuilder;

		public DynamicClassFactory()
			: this("Dynamic.Objects")
		{
		}

		public DynamicClassFactory(string assemblyName)
		{
			_appDomain = Thread.GetDomain();
			_assemblyName = assemblyName;
		}

		public Type CreateDynamicType(string name, Type basetype, Dictionary<string, Type> properties)
		{
			var tb = CreateDynamicTypeBuilder(name, basetype, properties);
			return tb.CreateType();
		}

		private TypeBuilder CreateDynamicTypeBuilder(string name, Type basetype, Dictionary<string, Type> properties)
		{
			if (_assemblyBuilder == null)
				_assemblyBuilder = _appDomain.DefineDynamicAssembly(new AssemblyName(_assemblyName), AssemblyBuilderAccess.RunAndSave);

			//ensure that namespace of the assembly is the same as the module name, else IL inspectors will fail
			if (_moduleBuilder == null)
				_moduleBuilder = _assemblyBuilder.DefineDynamicModule(_assemblyName + ".dll");

			//define new type derived from baseType
			var typeBuilder = _moduleBuilder.DefineType(_assemblyName + "." + name, TypeAttributes.Public
																				 | TypeAttributes.Class
																				 | TypeAttributes.AutoClass
																				 | TypeAttributes.AnsiClass
																				 | TypeAttributes.Serializable
																				 | TypeAttributes.BeforeFieldInit, basetype);

			//exclude base class properties
			var propertyInfos = basetype.GetProperties();
			foreach (var propertyInfo in propertyInfos)
			{
				var propertyName = propertyInfo.GetAttributeValue<ColumnAttribute, string>(column => column.Name) ?? propertyInfo.Name;
				properties.Remove(propertyName);
			}

			CreateProperties(typeBuilder, properties);

			return typeBuilder;
		}

		private void CreateProperties(TypeBuilder typeBuilder, Dictionary<string, Type> properties)
		{
			properties.ToList().ForEach(p => CreateFieldForType(typeBuilder, p.Value, p.Key));
		}

		private void CreateFieldForType(TypeBuilder typeBuilder, Type type, string name)
		{
			var fieldBuilder = typeBuilder.DefineField("_" + name.ToLowerInvariant(), type, FieldAttributes.Private);
			var propertyBuilder = typeBuilder.DefineProperty(name, PropertyAttributes.HasDefault, type, null);
			var getterAndSetterAttributes = MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig | MethodAttributes.Virtual;

			propertyBuilder.SetGetMethod(CreateGetMethod(typeBuilder, getterAndSetterAttributes, name, type, fieldBuilder));
			propertyBuilder.SetSetMethod(CreateSetMethod(typeBuilder, getterAndSetterAttributes, name, type, fieldBuilder));
		}

		private MethodBuilder CreateGetMethod(TypeBuilder typeBuilder, MethodAttributes attr, string name, Type type, FieldBuilder fieldBuilder)
		{
			var getMethodBuilder = typeBuilder.DefineMethod("get_" + name, attr, type, Type.EmptyTypes);

			var getMethodILGenerator = getMethodBuilder.GetILGenerator();
			getMethodILGenerator.Emit(OpCodes.Ldarg_0);
			getMethodILGenerator.Emit(OpCodes.Ldfld, fieldBuilder);
			getMethodILGenerator.Emit(OpCodes.Ret);

			return getMethodBuilder;
		}

		private MethodBuilder CreateSetMethod(TypeBuilder typeBuilder, MethodAttributes attr, string name, Type type, FieldInfo fieldBuilder)
		{
			var setMethodBuilder = typeBuilder.DefineMethod("set_" + name, attr, null, new[] { type });

			var setMethodILGenerator = setMethodBuilder.GetILGenerator();
			setMethodILGenerator.Emit(OpCodes.Ldarg_0);
			setMethodILGenerator.Emit(OpCodes.Ldarg_1);
			setMethodILGenerator.Emit(OpCodes.Stfld, fieldBuilder);

			setMethodILGenerator.Emit(OpCodes.Ret);

			return setMethodBuilder;
		}

		public void SaveAssembly()
		{
			if (_assemblyBuilder != null)
			{
				_assemblyBuilder.Save(_assemblyBuilder.GetName().Name + ".dll");
			}
		}
	}
}