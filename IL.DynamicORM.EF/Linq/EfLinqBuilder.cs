﻿using System;
using System.Linq.Expressions;
using IL.DynamicORM.Core.Linq;

namespace IL.DynamicORM.EF.Linq
{
	public static class EfLinqBuilder
	{
		public static Expression<Func<TBase, bool>> ConstructWherePredicate<TBase>(Type type, params LinqCriteriaField[] criteria)
		{
			return LinqBuilder.ConstructWherePredicate<TBase>(type, EPropertyExpression.Convert, criteria);
		}
	}
}
