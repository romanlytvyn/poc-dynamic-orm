﻿using System.Data.Entity;
using System.Linq;
using System.Reflection;
using IL.DynamicORM.Core.Linq;

namespace IL.DynamicORM.EF.Extensions
{
	public static class DbSetExtensions
	{
		public static object Max<TInstance>(this DbSet<TInstance> set, PropertyInfo propertyInfo) where TInstance : class
		{
			object maxValue = 0;

			if (set.Any())
			{
				if (propertyInfo.PropertyType == typeof(short))
				{
					maxValue = set.Max(LinqBuilder.ConstructSelector<TInstance, short>(propertyInfo.Name));
				}
				else if (propertyInfo.PropertyType == typeof(ushort))
				{
					maxValue = set.Max(LinqBuilder.ConstructSelector<TInstance, ushort>(propertyInfo.Name));
				}
				else if (propertyInfo.PropertyType == typeof(int))
				{
					maxValue = set.Max(LinqBuilder.ConstructSelector<TInstance, int>(propertyInfo.Name));
				}
				else if (propertyInfo.PropertyType == typeof(uint))
				{
					maxValue = set.Max(LinqBuilder.ConstructSelector<TInstance, uint>(propertyInfo.Name));
				}
				else if (propertyInfo.PropertyType == typeof(long))
				{
					maxValue = set.Max(LinqBuilder.ConstructSelector<TInstance, long>(propertyInfo.Name));
				}
				else if (propertyInfo.PropertyType == typeof(ulong))
				{
					maxValue = set.Max(LinqBuilder.ConstructSelector<TInstance, ulong>(propertyInfo.Name));
				}
				else if (propertyInfo.PropertyType == typeof(decimal))
				{
					maxValue = set.Max(LinqBuilder.ConstructSelector<TInstance, decimal>(propertyInfo.Name));
				}
			}

			return maxValue;
		}
	}
}
