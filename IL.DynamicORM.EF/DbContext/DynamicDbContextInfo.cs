﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using IL.DynamicORM.Core.Extensions;
using IL.DynamicORM.Core.Managers;
using IL.DynamicORM.Core.Providers;

namespace IL.DynamicORM.EF.DbContext
{
	public class DynamicDbContextInfo
	{
		private readonly string _key;

		private readonly string _entitiesNamespace;

		private readonly string _connectionString;

		private readonly object _lock = new object();

		public ORMInfoProvider OrmInfoProvider { get; private set; }

		public DbCompiledModel DbCompiledModel { get; private set; }

		public string ConnectionString { get; private set; }

		internal DynamicDbContextInfo(string key, string entitiesNamespace, string connectionString)
		{
			_key = key;
			_entitiesNamespace = entitiesNamespace;
			_connectionString = connectionString;

			Refresh();
		}

		public void Refresh(IEnumerable<string> tables = null)
		{
			lock (_lock)
			{
				var ormInfoProvider = OrmManager.GetProvider(_key, _entitiesNamespace, _connectionString, true, tables);

				var registedTypes = new HashSet<Type>();

				var modelBuilder = new DbModelBuilder();

				// register dynamic types and corresponding base types
				foreach (var table in ormInfoProvider.Tables)
				{
					modelBuilder.RegisterEntityType(table.Value.BaseType);
					modelBuilder.RegisterEntityType(table.Value);

					registedTypes.Add(table.Value.BaseType);
					registedTypes.Add(table.Value);
				}

				//register remaining types from entities namespace identified by [Table] attribute
				var allEntitiesTypes = AppDomain.CurrentDomain.GetAssemblies().SelectMany(t => t.GetTypes()).Where(t => t.IsClass && t.Namespace == _entitiesNamespace);
				foreach (var type in allEntitiesTypes)
				{
					if (!registedTypes.Contains(type) && !String.IsNullOrEmpty(type.GetAttributeValue<TableAttribute, string>(a => a.Name)))
					{
						modelBuilder.RegisterEntityType(type);
					}
				}

				using (var sqlConnection = new SqlConnection(_connectionString))
				{
					DbCompiledModel = modelBuilder.Build(sqlConnection).Compile();
					OrmInfoProvider = ormInfoProvider;
					ConnectionString = sqlConnection.ConnectionString;
				}
			}
		}
	}
}
