﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IL.DynamicORM.EF.DbContext
{
	public static class DynamicDbContextInfoFactory
	{
		private static readonly ConcurrentDictionary<string, DynamicDbContextInfo> DynamicDbContextInfos = new ConcurrentDictionary<string, DynamicDbContextInfo>();

		public static void Initialize(Dictionary<string, string> parameters, string entitiesNamespace)
		{
			Parallel.ForEach(parameters, parameter =>
			{
				DynamicDbContextInfos[parameter.Key] = new DynamicDbContextInfo(parameter.Key, entitiesNamespace, parameter.Value);
			});
		}

		public static DynamicDbContextInfo GetDynamicDbContextInfo(string key)
		{
			return DynamicDbContextInfos[key];
		}

		public static void RefreshDynamicDbContextInfo(string key, IEnumerable<string> tables = null)
		{
			DynamicDbContextInfos[key].Refresh(tables);
		}
	}
}
