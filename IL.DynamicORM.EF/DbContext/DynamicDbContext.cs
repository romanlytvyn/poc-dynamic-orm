using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using IL.DynamicORM.Core.Extensions;
using IL.DynamicORM.Core.Helpers;
using IL.DynamicORM.Core.Providers;
using IL.DynamicORM.EF.Extensions;

namespace IL.DynamicORM.EF.DbContext
{
	public abstract class DynamicDbContext : System.Data.Entity.DbContext
	{
		private readonly ORMInfoProvider _ormInfoProvider;

		protected DynamicDbContext(DynamicDbContextInfo contextInfo)
			: base(contextInfo.ConnectionString, contextInfo.DbCompiledModel)
		{
			_ormInfoProvider = contextInfo.OrmInfoProvider;
		}

		public TBase CreateInstance<TBase>()
		{
			return _ormInfoProvider.CreateInstance<TBase>();
		}

		public bool SaveNewInstance<TInstance>(TInstance instance) where TInstance : class
		{
			// test with nested transactions and concurrency
			using (var transaction = Database.BeginTransaction(System.Data.IsolationLevel.RepeatableRead))
			{
				try
				{
					var set = Set<TInstance>();
					var keyPropertyInfo = typeof(TInstance).GetPropertyInfoByAttribute<KeyAttribute>();

					if (keyPropertyInfo == null)
					{
						throw new MissingMemberException("Missing [Key] attribute on primary key property");
					}

					var maxKeyValue = set.Max(keyPropertyInfo);
					var newKey = MathHelper.Increment(maxKeyValue);

					keyPropertyInfo.SetValue(instance, newKey);

					set.Add(instance);

					SaveChanges();
					transaction.Commit();

					return true;
				}
				catch (Exception)
				{
					transaction.Rollback();
					return false;
				}
			}
		}

		public Type GetActualType<TBase>()
		{
			return _ormInfoProvider.GetActualType<TBase>();
		}

		protected DbSet<TBase> DynamicSet<TBase>() where TBase : class
		{
			var actualInstance = CreateInstance<TBase>();
			return ActualDynamicSet(actualInstance);
		}

		// ReSharper disable once UnusedParameter.Local
		private DbSet<TActual> ActualDynamicSet<TActual>(TActual instance) where TActual : class
		{
			return Set<TActual>();
		}
	}
}
